'use strict';

const electron = require('electron');
const app = electron.app;
const BrowserWindow = electron.BrowserWindow;

// electron.crashReporter.start();

let mainWindow = null;

app.on('window-all-closed', () => {
    app.quit();
});

app.on('ready', () => {
    let subgo = require('child_process').spawn('./backend/bin/moviedb');
    let rp = require('request-promise');
    let mainAddr = 'http://localhost:5000';

    function openWindow () {
        mainWindow = new BrowserWindow({width: 1000, height: 800});
        mainWindow.loadURL(mainAddr);
        mainWindow.webContents.openDevTools();
        mainWindow.on('closed', () => {
            mainWindow = null;
            subgo.kill('SIGINT');
        });
    }

    let startTime = new Date();

    function startUp () {
        rp(mainAddr)
        .then(() => {
            console.log('server running @', mainAddr);
            openWindow();
        })
        .catch(() => {
            let endTime = new Date();
            let timeDiff = endTime - startTime;
            if (timeDiff > 5000) {
                console.log('couldn\'t start server');
                // TODO how to kill process?
            } else {
                startUp();
            }
        });
    }

    console.log('waiting for server to start...');
    startUp();
});
