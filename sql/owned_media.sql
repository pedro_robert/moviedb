CREATE TABLE owned_media (
    _id INT AUTO_INCREMENT,
    collection_id INT,
    media_id VARCHAR(10),
    media_type VARCHAR(10),
    media_status ENUM('watched','intended'),
    disk_space FLOAT DEFAULT '0.0',
    FOREIGN KEY (movie_id)
        REFERENCES all_media (imdb_id)
        ON UPDATE CASCADE
        ON DELETE CASCADE,
    FOREIGN KEY (collection_id)
        REFERENCES collections (_id)
        ON UPDATE CASCADE
        ON DELETE CASCADE,
    PRIMARY KEY (_id)
);
