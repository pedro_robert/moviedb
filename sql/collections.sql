CREATE TABLE collections (
    _id INT AUTO_INCREMENT,
    name VARCHAR (20),
    description TEXT,
    PRIMARY KEY (_id)
);
