GB=$(GOPATH)/bin/gb
ELECTRON=electron
RM=rm -rf

.PHONY: all clean backend test electron

all: backend electron
setup: vendor-update backend electron

vendor-update:
	cd backend && $(GB) vendor update --all

backend:
	cd backend && $(GB) build

test:
	cd backend && $(GB) test -v

electron: backend
	npm start

server: backend
	./backend/bin/moviedb

clean:
	$(RM) backend/bin backend/pkg
