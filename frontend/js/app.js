'use strict';

let app = angular.module('moviedbApp', ['ngRoute', 'btford.modal']);

app.config(function ($routeProvider) {
    $routeProvider
        .when('/', {
            controller: 'MainController',
            templateUrl: 'frontend/views/getStarted.html'
        })
        .when('/view/:id', {
            controller: 'ViewController',
            templateUrl: 'frontend/views/view.html'
        })
        .when('/addMedia/:id', {
            controller: 'AddMediaController',
            templateUrl: 'frontend/views/addMedia.html'
        })
        .when('/rawQueries/:id', {
            controller: 'RawQueriesController',
            templateUrl: 'frontend/views/rawQueries.html'
        })
        .otherwise({
            redirectTo: '/'
        }
    );
});
