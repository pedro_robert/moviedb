'use strict';

app.controller('AddMediaController', [
    '$scope', '$location', '$routeParams', 'dbInstance', 'omdbQuery',
    'apiQueries', 'addModal', 'parseMedia', 'activeTab', function ($scope,
    $location, $routeParams, dbInstance, omdbQuery, apiQueries, addModal,
    parseMedia, activeTab) {
        // tries to retrieve id
        // first from url, than from instance service
        // else redirects to getStarted
        if ($routeParams.id) {
            dbInstance.setInstance($routeParams.id);
            $scope.dbId = dbInstance.getInstance();
        } else if (dbInstance.getInstance()) {
            $scope.dbId = dbInstance.getInstance();
        } else {
            $location.path('/');
        }
        activeTab.changeTab(2);
        $scope.validMedia = false;
        $scope.searchMovie = () => {
            if ($scope.movieName) {
                omdbQuery
                    .getMovie($scope.movieName)
                    .then((res) => {
                        console.log(res);
                        if (res.data.Response === 'False') {
                            $scope.movieName = res.data.Error;
                        } else {
                            parseMedia.add(res.data);
                            $scope.parsedMedia = parseMedia.getMedia().Media;
                            $scope.validMedia = true;
                        }
                    })
                    .catch((err) => {
                        console.log(err);
                        $scope.validMedia = false;
                    });
            }
        };
        $scope.openModal = () => {
            if ($scope.validMedia) {
                addModal.activate();
            }
        };
        $scope.hideModal = () => {
            addModal.deactivate();
        };
    }]
);
