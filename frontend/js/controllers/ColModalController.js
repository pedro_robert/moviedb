'use strict';

app.controller('ColModalController', [
    '$scope', '$location', 'apiQueries', 'colModal',
    function ($scope, $location, apiQueries, colModal) {
        apiQueries.getAllCollections()
            .then((res) => {
                if (res.status === 200 && res.data.status === 'success') {
                    $scope.colList = res.data.data;
                }
            })
            .catch((err) => console.log(err));
        $scope.selectCollection = (col) => {
            $scope.hideModal();
            $location.path('view/' + col.ID);
        };
        $scope.hideModal = () => colModal.deactivate();
    }]
);
