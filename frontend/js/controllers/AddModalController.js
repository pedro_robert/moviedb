'use strict';

app.controller('AddModalController', [
    '$scope', 'apiQueries', 'addModal', 'parseMedia', 'dbInstance',
    function ($scope, apiQueries, addModal, parseMedia, dbInstance) {
        $scope.mediaType = [
            'DVD', 'Hard Drive', 'Bluray', 'Netflix', 'Other'
        ];
        $scope.mediaStatus = [
            'Watched', 'Planned'
        ];
        $scope.selectedType = $scope.mediaType[0];
        $scope.selectedStatus = $scope.mediaStatus[0];
        $scope.diskSpace = '';
        $scope.addMedia = () => {
            let info = {};
            info['MediaType'] = $scope.selectedType;
            info['MediaStatus'] = $scope.selectedStatus;
            info['DiskSpace'] = $scope.diskSpace ? $scope.diskSpace : '0';
            parseMedia.addInfo(info);
            let clearMedia = parseMedia.getMedia();
            delete clearMedia['imdbRatingParse'];
            delete clearMedia['Response'];
            apiQueries.addMedia(
                dbInstance.getInstance(), clearMedia
            )
            .then((res) => {
                if (res.status === 200 && res.data.status === 'success') {
                    $scope.hideModal();
                } else {
                    console.log(res);
                }
            })
            .catch((err) => {
                console.log(err);
            });
        };
        $scope.hideModal = () => addModal.deactivate();
    }]
);
