'use strict';

app.controller('RawQueriesController', [
    '$scope', '$location', '$routeParams', 'dbInstance', 'activeTab',
    'apiQueries', function ($scope, $location, $routeParams, dbInstance,
    activeTab, apiQueries) {
        // tries to retrieve id
        // first from url, than from instance service
        // else redirects to getStarted
        if ($routeParams.id) {
            dbInstance.setInstance($routeParams.id);
            $scope.dbId = dbInstance.getInstance();
        } else if (dbInstance.getInstance()) {
            $scope.dbId = dbInstance.getInstance();
        } else {
            $location.path('/');
        }
        activeTab.changeTab(3);
        $scope.queryString = '';
        $scope.queryResponse;
        $scope.execQuery = () => {
            if ($scope.queryString.match('select') ||
                $scope.queryString.match('SELECT')) {
                apiQueries.rawQuery($scope.queryString)
                    .then((res) => {
                        if (res.status === 200 &&
                            res.data.status === 'success') {
                            $scope.queryResponse = res.data.data;
                        }
                    })
                    .catch((err) => {
                        console.log(err);
                    });
            }
        };
    }]
);
