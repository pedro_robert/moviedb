'use strict';

app.controller('ViewController', [
    '$scope', '$location', '$routeParams', 'dbInstance', 'activeTab',
    'apiQueries', 'parseMedia', function ($scope, $location, $routeParams,
    dbInstance, activeTab, apiQueries, parseMedia) {
        // tries to retrieve id
        // first from url, than from instance service
        // else redirects to getStarted
        if ($routeParams.id) {
            dbInstance.setInstance($routeParams.id);
            $scope.dbId = dbInstance.getInstance();
        } else if (dbInstance.getInstance()) {
            $scope.dbId = dbInstance.getInstance();
        } else {
            $location.path('/');
        }
        activeTab.changeTab(1);
        $scope.mediaStatus = '';
        $scope.changeStatus = (status) => {
            $scope.mediaStatus = status;
            let colID = $scope.activeMedia.CollectionID;
            let mediaID = $scope.activeMedia.MediaID;
            apiQueries.rawQuery(
                `update owned_media set media_status = '${status}'
                where collection_id = '${colID}' and media_id = '${mediaID}'`
            ).then((res) => {
                console.log(res);
                $scope.activeMedia.MediaStatus = status;
            })
            .catch((err) => {
                console.log(err);
            });
        };
        $scope.collection = {};
        apiQueries.getCollection($scope.dbId)
            .then((res) => {
                if (res.status === 200 && res.data.status === 'success') {
                    $scope.collection = res.data.data[0];
                }
            })
            .catch((err) => {
                console.log(err);
                $location.path('/');
            });
        function jsonConcat(o1, o2) {
            for (let key in o2) {
                o1[key] = o2[key];
            }
        }
        $scope.displayMedia = (media) => {
            $scope.activeMedia = media;
            $scope.activeMedia['imdbRatingParse'] = Math.round(
                Number(media.imdbRating)/2
            );
            $scope.mediaStatus = media.MediaStatus;
        };
        function findIdx(find, array) {
            let idx;
            find = find.replace(/ /g, '');
            array.forEach((element, i) => {
                if (find.match(element.name.replace(/ /g, ''))) {
                    idx = i;
                }
            });
            return idx;
        }
        function getTopCountry(it) {
            if (it < $scope.colMedia.length) {
                return;
            }
            let countries = [];
            $scope.colMedia.forEach((media) => {
                let idx = findIdx(media.Country, countries);
                if (idx) {
                    countries[idx].count++;
                } else {
                    countries.push({
                        name: media.Country,
                        count: 1
                    });
                }
            });
            $scope.sortedCountries = countries.slice();
            $scope.sortedCountries.sort((a, b) => b.count > a.count ? 1 : 0);
            $scope.topCountry = $scope.sortedCountries[0];
        }
        function getTopDirector(it) {
            if (it < $scope.colMedia.length) {
                return;
            }
            let directors = [];
            $scope.colMedia.forEach((media) => {
                let idx = findIdx(media.Director, directors);
                if (idx) {
                    directors[idx].count++;
                } else {
                    directors.push({
                        name: media.Director,
                        count: 1
                    });
                }
            });
            $scope.sortedDirectors = directors.slice();
            $scope.sortedDirectors.sort((a, b) => b.count > a.count ? 1 : 0);
            $scope.topDirector = $scope.sortedDirectors[0];
        }
        apiQueries.getColMedia($scope.dbId)
            .then((res) => {
                if (res.status === 200 && res.data.status === 'success') {
                    $scope.colMedia = res.data.data;
                }
                if ($scope.colMedia && $scope.colMedia.length > 0) {
                    let it = 1;
                    $scope.colMedia.forEach((media) => {
                        apiQueries.getMedia($scope.dbId, media.MediaID)
                            .then((res) => {
                                if (res.status === 200 &&
                                    res.data.status === 'success') {
                                    let unparsed = parseMedia.unparseMedia(
                                        res.data.data.Media
                                    );
                                    jsonConcat(media, unparsed);
                                    getTopCountry(it);
                                    getTopDirector(it);
                                    it++;
                                }
                            })
                            .catch((err) => {
                                console.log(err);
                            });
                    });
                }
            })
            .catch((err) => {
                console.log(err);
            });
    }]
);
