'use strict';

app.controller('MainController', ['$scope', '$location', '$routeParams',
    'omdbQuery', 'apiQueries', 'colModal', 'dbInstance',
    'activeTab', function ($scope, $location, $routeParams, omdbQuery,
    apiQueries, colModal, dbInstance, activeTab) {
        $scope.colList;
        activeTab.changeTab(0);
        $scope.startString = 'Welcome to moviedbApp';
        $scope.$watch(dbInstance.getActiveTab, (id) => {
            if (id) {
                $scope.startString = 'connected to ' + id;
            }
        });
        $scope.currentTab = activeTab.getActiveTab();
        $scope.$watch(activeTab.getActiveTab, (tab) => $scope.currentTab = tab);
        $scope.openCollection = 0;
        $scope.addOrOpenCollection = (activeTab) => {
            $scope.openCollection = activeTab;
        };
        $scope.openModal = () => {
            colModal.activate();
        };
        $scope.hideModal = () => {
            colModal.deactivate();
        };
        $scope.view = () => {
            if (dbInstance.getInstance()) {
                activeTab.changeTab(1);
                $location.path('/view/' + dbInstance.getInstance());
            }
        };
        $scope.addMedia = () => {
            if (dbInstance.getInstance()) {
                activeTab.changeTab(2);
                $location.path('/addMedia/' + dbInstance.getInstance());
            }
        };
        $scope.rawQueries = () => {
            if (dbInstance.getInstance()) {
                activeTab.changeTab(3);
                $location.path('/rawQueries/' + dbInstance.getInstance());
            }
        };
        $scope.connectToDB = () => {
            if (!$scope.dbDescription) {
                $scope.dbDescription = 'no description :(';
            }
            apiQueries
                .addCollection($scope.dbName, $scope.dbDescription)
                .then((res) => {
                    if (res.status === 200 &&
                        res.data.status === 'success') {
                        dbInstance.setInstance(res.data.data);
                        $location.path('/view/' + dbInstance.getInstance());
                        activeTab.changeTab(1);
                    } else {
                        console.log('couldn\'t create collection');
                    }
                })
                .catch((err) => console.log(err));
        };
    }]
);
