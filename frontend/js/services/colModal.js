'use strict';

app.factory('colModal', function (btfModal) {
    return btfModal({
        controller: 'ColModalController',
        templateUrl: 'frontend/views/colModal.html'
    });
});
