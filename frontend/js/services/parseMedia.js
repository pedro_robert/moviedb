'use strict';

app.factory('parseMedia', function () {
    let owned_media = {};
    return {
        add: function (data) {
            data.imdbRatingParse = `${Math.round(data.imdbRating/2)}`;
            data['imdbVotes'] = data['imdbVotes'].replace(/,/, '');
            data['Runtime'] = data['Runtime'].replace(/ min/, '');
            let media = {};
            for (let i in data) {
                let valid = data[i] !== '';
                let prop = {};
                if (i === 'Released') {
                    prop['Time'] = data[i];
                } else if (i === 'imdbRating'){
                    prop['Float64'] = Number(data[i]);
                } else if(isNaN(data[i])) {
                    prop['String'] = data[i];
                } else {
                    prop['Int64'] = Number(data[i]);
                }
                prop['Valid'] = valid;
                media[i] = prop;
            }
            owned_media['Media'] = media;
        },
        addInfo: function (info) {
            owned_media['OwnInfo'] = info;
        },
        unparseMedia: function (media) {
            let unparsed = {};
            for (let key in media) {
                for (let subKey in media[key]) {
                    if (subKey !== 'Valid') {
                        unparsed[key] = media[key][subKey];
                    }
                }
            }
            return unparsed;
        },
        getMedia: function () {
            return owned_media;
        }
    };
});
