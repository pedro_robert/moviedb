'use strict';

app.factory('apiQueries', ['$http', function ($http) {
    return {
        addCollection: function (name, desc) {
            let req = {
                method: 'POST',
                url: 'http://localhost:5000/api/col/add',
                headers: {
                    'Content-Type': 'application/json'
                },
                data: {
                    'Name': name,
                    'Description': desc
                }
            };
            return $http(req);
        },
        getAllCollections: function () {
            let req = {
                method: 'GET',
                url: 'http://localhost:5000/api/col/all'
            };
            return $http(req);
        },
        addMedia: function (id, media) {
            let req = {
                method: 'POST',
                url: 'http://localhost:5000/api/col/' + id + '/media/add',
                headers: {
                    'Content-Type': 'application/json'
                },
                data: media
            };
            return $http(req);
        },
        rawQuery: function (query) {
            let req = {
                method: 'GET',
                url: 'http://localhost:5000/api/raw?query=' + query
            };
            return $http(req);
        },
        getCollection: function (id) {
            let req = {
                method: 'GET',
                url: 'http://localhost:5000/api/col/' + id
            };
            return $http(req);
        },
        getColMedia: function (id) {
            let req = {
                method: 'GET',
                url: `http://localhost:5000/api/col/${id}/media/all`
            };
            return $http(req);
        },
        getMedia: function (colId, mediaId) {
            let req = {
                method: 'GET',
                url: `http://localhost:5000/api/col/${colId}/media/${mediaId}`
            };
            return $http(req);
        }
    };
}]);
