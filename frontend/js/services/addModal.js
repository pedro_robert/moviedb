'use strict';

app.factory('addModal', function (btfModal) {
    return btfModal({
        controller: 'AddModalController',
        templateUrl: 'frontend/views/addModal.html'
    });
});
