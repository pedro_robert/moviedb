'use strict';

app.factory('dbInstance', function () {
    let dbId;
    return {
        setInstance: function (id) {
            dbId = id;
        },
        getInstance: function () {
            return dbId;
        }
    };
});
