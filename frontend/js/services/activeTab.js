'use strict';

app.factory('activeTab', function () {
    let activeTab = 0;
    return {
        changeTab: function (tab) {
            activeTab = tab;
        },
        getActiveTab: function () {
            return activeTab;
        }
    };
});
