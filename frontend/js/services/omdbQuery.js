'use strict';

app.factory('omdbQuery', ['$http', function($http) {
    return {
        getMovie: function (name) {
            let parseName = name.replace(/\s/, '+');
            let req = {
                method: 'GET',
                url: 'http://www.omdbapi.com/?t=' + parseName
                    + '&y=&plot=short&r=json'
            };
            return $http(req);
        }
    };
}]);
