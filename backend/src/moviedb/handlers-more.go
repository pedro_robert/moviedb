package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"moviedb/omdb"
	"net/http"

	"github.com/go-zoo/bone"
)

func HelperParseToStringMap(m map[string]interface{}) map[string]string {
	var newMap = make(map[string]string)
	for k, v := range m {
		newMap[k] = fmt.Sprintf("%s", v)
	}
	return newMap
}

func NotPostWarningHandler(w http.ResponseWriter, req *http.Request) {
	err := errors.New("This endpoint receives data but you did a GET request. You should POST instead.")
	resp := apiError(err)
	w.Write(resp.ToJSON())
}

func SearchOmdbHandler(w http.ResponseWriter, req *http.Request) {
	var title string = bone.GetValue(req, "title")
	r, err := omdb.Query(title, "")
	if err != nil {
		resp := apiError(err)
		w.Write(resp.ToJSON())
	}
	// this should probably be changed to encode from db.Media instead of from oDBconn.Response
	json.NewEncoder(w).Encode(r)
}

func RawQueryHandler(w http.ResponseWriter, r *http.Request) {
	query := r.URL.Query().Get("query")
	log.Println(query)

	rowx, err := DB.Conn.Queryx(query)
	if err != nil {
		resp := apiError(err)
		w.Write(resp.ToJSON())
		return
	}

	var scannedMaps []map[string]string
	for rowx.Next() {
		var rowxMap = make(map[string]interface{})
		err = rowx.MapScan(rowxMap)
		if err != nil {
			log.Fatal(err)
		}
		stringMap := HelperParseToStringMap(rowxMap)
		scannedMaps = append(scannedMaps, stringMap)
	}
	resp := NewResponse(true)
	resp.SetData(scannedMaps)
	w.Write(resp.ToJSON())
}
