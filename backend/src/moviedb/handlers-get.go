package main

import (
	"moviedb/db"
	"net/http"

	"github.com/go-zoo/bone"
)

func GetAllCollectionsHandler(w http.ResponseWriter, req *http.Request) {
	var cols []db.Collection
	err := DB.Conn.Select(&cols, "SELECT * FROM "+CollectionTable)
	if err != nil {
		resp := apiError(err)
		w.Write(resp.ToJSON())
		return
	}

	resp := NewResponse(cols != nil)
	resp.SetData(cols)
	w.Write(resp.ToJSON())
}

func GetCollectionHandler(w http.ResponseWriter, req *http.Request) {
	var cols []db.Collection
	err := DB.Conn.Select(&cols, "SELECT * FROM "+CollectionTable+" WHERE _id=?", bone.GetValue(req, "cid"))
	if err != nil {
		resp := apiError(err)
		w.Write(resp.ToJSON())
		return
	}

	resp := NewResponse(cols != nil)
	resp.SetData(cols)
	w.Write(resp.ToJSON())
}

func GetAllMediaHandler(w http.ResponseWriter, req *http.Request) {
	var oms []db.OwnedMedia
	err := DB.Conn.Select(&oms, "SELECT * FROM "+OwnedMediaTable+" WHERE "+CollectionID+"=?", bone.GetValue(req, "cid"))
	if err != nil {
		resp := apiError(err)
		w.Write(resp.ToJSON())
		return
	}

	resp := NewResponse(oms != nil)
	resp.SetData(oms)
	w.Write(resp.ToJSON())
}

func GetMediaHandler(w http.ResponseWriter, req *http.Request) {
	urlVals := bone.GetAllValues(req)

	var media_data []*db.Media
	err := DB.Conn.Select(&media_data, `SELECT am.*
		FROM owned_media AS om
		INNER JOIN all_media AS am
		ON om.media_id = am.imdb_id
		WHERE om.collection_id=? AND om.media_id=?
		`, urlVals["cid"], urlVals["mid"])

	if err != nil {
		resp := apiError(err)
		w.Write(resp.ToJSON())
		return
	}

	var owned_data []*db.OwnedMedia
	err = DB.Conn.Select(&owned_data, `SELECT om.*
		FROM owned_media AS om
		INNER JOIN all_media AS am
		ON om.media_id = am.imdb_id
		WHERE om.collection_id=? AND om.media_id=?
	`, urlVals["cid"], urlVals["mid"])

	if err != nil {
		resp := apiError(err)
		w.Write(resp.ToJSON())
		return
	}

	if media_data == nil || owned_data == nil {
		resp := apiError(err)
		w.Write(resp.ToJSON())
		return
	}

	listView := &db.ListView{
		Media:   *(media_data[0]),
		OwnInfo: *(owned_data[0]),
	}

	resp := NewResponse(true)
	resp.SetData(*listView)
	w.Write(resp.ToJSON())
}

func RemoveMediaHandler(w http.ResponseWriter, r *http.Request) {
	result := DB.Conn.MustExec(`DELETE FROM owned_media WHERE collection_id=? AND media_id=?`, bone.GetValue(r, "cid"), bone.GetValue(r, "mid"))

	lastInsertID, _ := result.LastInsertId()
	resp := NewResponse(true)
	resp.SetData(lastInsertID)
	w.Write(resp.ToJSON())
}
