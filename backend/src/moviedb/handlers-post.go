package main

import (
	"encoding/json"
	"errors"
	"io"
	"log"
	"moviedb/db"
	"net/http"
	"strconv"

	"github.com/go-sql-driver/mysql"
	"github.com/go-zoo/bone"
)

func AddCollectionHandler(w http.ResponseWriter, req *http.Request) {
	if req.ContentLength > 0 {
		c := make([]byte, req.ContentLength)
		_, err := req.Body.Read(c)
		defer req.Body.Close()

		if err == io.EOF {
			err = nil
		}

		if err != nil {
			log.Panic(err)
		}

		var newCol db.Collection
		err = json.Unmarshal(c, &newCol)
		if err != nil {
			log.Panic(err)
		}

		// check for empty data
		if newCol.Name == "" || newCol.Description == "" {
			resp := apiError(errors.New("Empty field in POST body"))
			w.Write(resp.ToJSON())
			return
		}

		result := DB.Conn.MustExec(`INSERT INTO collections (name, description) VALUE (?, ?)`, newCol.Name, newCol.Description)

		resp := NewResponse(true)
		lastInsertID, _ := result.LastInsertId()
		resp.SetData(lastInsertID)
		w.Write(resp.ToJSON())
	} else {
		resp := apiError(errors.New("Empty POST body"))
		w.Write(resp.ToJSON())
	}
}

func AddMediaHandler(w http.ResponseWriter, req *http.Request) {
	if req.ContentLength > 0 {
		c := make([]byte, req.ContentLength)
		_, err := req.Body.Read(c)
		defer req.Body.Close()

		if err == io.EOF {
			err = nil
		}

		if err != nil {
			log.Panic(err)
		}

		var newListView db.ListView
		err = json.Unmarshal(c, &newListView)
		if err != nil {
			log.Panic(err)
		}

		// check for empty data
		if newListView.Media.ImdbID.String == "" ||
			newListView.OwnInfo.MediaType == "" ||
			newListView.OwnInfo.DiskSpace == "" {
			resp := apiError(errors.New("Empty field in POST body"))
			log.Println(newListView)
			w.Write(resp.ToJSON())
			return
		}
		newListView.OwnInfo.MediaID = newListView.Media.ImdbID.String
		newListView.OwnInfo.CollectionID, _ = strconv.Atoi(bone.GetValue(req, "cid"))

		tx := DB.Conn.MustBegin()

		insertAllMediaStmt, err := tx.PrepareNamed(`
		INSERT INTO all_media (imdb_id, imdb_votes, imdb_rating, metascore, title, poster, year,
			genre, released, actors, director, writer, plot, language, country, rated, runtime)
		VALUES (:imdb_id, :imdb_votes, :imdb_rating, :metascore, :title, :poster, :year,
			:genre, :released, :actors, :director, :writer, :plot, :language, :country, :rated, :runtime)`)
		insertOwnedMediaStmt, err := tx.PrepareNamed(`
		INSERT INTO owned_media (collection_id, media_id, media_type, disk_space, media_status)
		VALUES (:collection_id, :media_id, :media_type, :disk_space, :media_status)`)

		log.Println(newListView)

		_, err = insertAllMediaStmt.Exec(newListView.Media)
		if err != nil {
			// errors because of table definition, all_media entries remain on remove
			if !(err.(*mysql.MySQLError).Number == db.DUBENTRY) {
				resp := apiError(err)
				w.Write(resp.ToJSON())
				return
			}
		}
		_, err = insertOwnedMediaStmt.Exec(newListView.OwnInfo)
		if err != nil {
			resp := apiError(err)
			w.Write(resp.ToJSON())
			return
		}

		err = tx.Commit()
		if err != nil {
			resp := apiError(err)
			w.Write(resp.ToJSON())
			return
		}

		resp := NewResponse(true)
		resp.SetData(newListView.Media.ImdbID.String)
		w.Write(resp.ToJSON())
	} else {
		resp := apiError(errors.New("Empty POST body"))
		w.Write(resp.ToJSON())
	}
}

func UpdateMediaHandler(w http.ResponseWriter, req *http.Request) {
	// UPDATE...
	resp := apiError(errors.New("not yet"))
	w.Write(resp.ToJSON())
}
