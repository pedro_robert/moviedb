package db

import (
	"os"

	_ "github.com/go-sql-driver/mysql"
	"github.com/jmoiron/sqlx"
)

const (
	DUBENTRY = 1062
	DBtype   = "mysql"
)

var DBcreds = os.Getenv("MOVIEDB_CONNECTION")

type MovieDB struct {
	Conn sqlx.DB
}

func DBConnect() MovieDB {
	db := sqlx.MustConnect(DBtype, DBcreds)
	return MovieDB{
		Conn: *db,
	}
}
