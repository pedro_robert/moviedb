package db

import (
	"testing"
	"time"
)

var (
	expectedNullTime = NullTime{
		Time:  time.Date(2016, time.June, 28, 00, 0, 0, 0, time.UTC),
		Valid: true,
	}
	expectedJSON = []byte("{\"Time\": \"28 Jun 2016\", \"Valid\": true}")
)

func TestNullTimeUnmarshaler(t *testing.T) {
	got := NullTime{}
	got.UnmarshalJSON(expectedJSON)

	if got != expectedNullTime {
		t.Fatalf("not the same\nwant:\t%v\ngot\t%v\n", expectedNullTime, got)
	}
}

func TestNullTimeMarshaler(t *testing.T) {
	var got []byte
	got, err := expectedNullTime.MarshalJSON()
	if err != nil {
		t.Fatalf("TestMarshalNullTime failed because of Marshal error, %v", err)
	}

	if string(got) != string(expectedJSON) {
		t.Fatalf("not the same\nwant:\t%s\ngot\t%s\n", expectedJSON, got)
	}
}

func TestNullTimeScanner(t *testing.T) {
	t.Skip("Don't know how to test this yet")
}

func TestNullTimeValuer(t *testing.T) {
	t.Skip("Don't know how to test this yet")
}
