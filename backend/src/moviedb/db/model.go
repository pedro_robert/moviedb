package db

import "database/sql"

type Media struct {
	ImdbID     sql.NullString  `json:"imdbID" db:"imdb_id"`
	ImdbRating sql.NullFloat64 `json:"imdbRating" db:"imdb_rating"`
	ImdbVotes  sql.NullInt64   `json:"imdbVotes" db:"imdb_votes"`
	Metascore  sql.NullInt64   `json:"Metascore" db:"metascore"`
	Runtime    sql.NullInt64   `json:"Runtime" db:"runtime"`
	Year       sql.NullInt64   `json:"Year" db:"year"`
	Title      sql.NullString  `json:"Title" db:"title"`
	Released   NullTime        `json:"Released" db:"released"`
	Poster     sql.NullString  `json:"Poster" db:"poster"`
	Director   sql.NullString  `json:"Director" db:"director"`
	Writer     sql.NullString  `json:"Writer" db:"writer"`
	Actors     sql.NullString  `json:"Actors" db:"actors"`
	Rated      sql.NullString  `json:"Rated" db:"rated"`
	Genre      sql.NullString  `json:"Genre" db:"genre"`
	Language   sql.NullString  `json:"Language" db:"language"`
	Country    sql.NullString  `json:"Country" db:"country"`
	Plot       sql.NullString  `json:"Plot" db:"plot"`
}

type OwnedMedia struct {
	ID           int    `json:"-" db:"_id"`
	CollectionID int    `json:"CollectionID" db:"collection_id"`
	MediaID      string `json:"MediaID" db:"media_id"`
	MediaType    string `json:"MediaType" db:"media_type"`
	MediaStatus  string `json:"MediaStatus" db:"media_status"`
	DiskSpace    string `json:"DiskSpace" db:"disk_space"`
}

type Collection struct {
	ID          int    `json:"ID,omitempty" db:"_id"`
	Name        string `json:"Name" db:"name"`
	Description string `json:"Description" db:"description"`
}

type ListView struct {
	Media   Media
	OwnInfo OwnedMedia
}
