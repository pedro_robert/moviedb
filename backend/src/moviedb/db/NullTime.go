package db

import (
	"database/sql/driver"
	"encoding/json"
	"fmt"
	"io"
	"strings"
	"time"
)

const omdbform = "_2 Jan 2006"

type NullTime struct {
	Time  time.Time
	Valid bool
}

// MarshalJSON implements encoding/json Marshaler
func (nt *NullTime) MarshalJSON() ([]byte, error) {
	stamp := fmt.Sprintf("{\"Time\": \"%s\", \"Valid\": %v}", time.Time(nt.Time).Format(omdbform), nt.Valid)
	return []byte(stamp), nil
}

// UnmarshalJSON implements encoding/json Unmarshaler
func (nt *NullTime) UnmarshalJSON(data []byte) error {
	dec := json.NewDecoder(strings.NewReader(string(data)))
	for {
		t, err := dec.Token()
		if err == io.EOF {
			break
		}
		if err != nil {
			panic(err)
		}
		switch t.(type) {
		case string:
			s := t.(string)
			if s == "true" {
				nt.Valid = true
			}
			if s == "false" {
				nt.Valid = false
			}
			if s != "Time" && s != "Valid" {
				theTime, err := time.Parse(omdbform, t.(string))
				if err != nil {
					return err
				}
				nt.Time = theTime
			}
		case bool:
			nt.Valid = t.(bool)
		default:
			break
		}
	}
	return nil
}

// Scan implements the Scanner interface.
func (nt *NullTime) Scan(value interface{}) error {
	nt.Time, nt.Valid = value.(time.Time)
	return nil
}

// Value implements the driver Valuer interface.
func (nt NullTime) Value() (driver.Value, error) {
	if !nt.Valid {
		return nil, nil
	}
	return nt.Time, nil
}
