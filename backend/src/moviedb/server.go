package main

import (
	"fmt"
	"log"
	"net/http"

	"github.com/go-zoo/bone"
)

const PORT string = "5000"

// mainHandler is index handler
func mainHandler(w http.ResponseWriter, req *http.Request) {
	http.ServeFile(w, req, "frontend/index.html")
}

// staticHandler serves static files
func staticHandler(w http.ResponseWriter, req *http.Request) {
	// log.Println(req.URL.Path[1:])
	fs := http.FileServer(http.Dir("./frontend/"))
	http.StripPrefix("/frontend/", fs).ServeHTTP(w, req)
}

// Project main
func main() {
	defer func() {
		if r := recover(); r != nil {
			fmt.Println(">> we had a panic sir:", r)
		}
	}()

	mux := bone.New()
	mux.Handle("/", http.HandlerFunc(mainHandler))
	mux.Handle("/frontend/", http.HandlerFunc(staticHandler))

	// API routing moved to api package
	mux.SubRoute("/api", NewAPIRouter())

	log.Printf("server: serving on port %v", PORT)
	err := http.ListenAndServe(":"+PORT, mux)
	if err != nil {
		log.Fatal("server: ListenAndServe failed, ", err)
	}
}
