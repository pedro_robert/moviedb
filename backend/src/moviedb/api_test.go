package main

import (
	"encoding/json"
	"errors"
	"testing"
)

func TestAPIError(t *testing.T) {
	err := ResponseError{Error: "TestError"}
	want := Response{
		Status: false,
		Data:   err,
	}
	got := apiError(errors.New("TestError"))
	if *got != want {
		t.Errorf("Got: %v\nWanted: %v", got, want)
	}

	want_json := []byte("{\"status\":\"failure\",\"data\":{\"error\":\"TestError\"}}")
	got_json, _ := json.Marshal(got)
	if string(want_json) != string(got_json) {
		t.Logf("Got:\t%s\n", got_json)
		t.Logf("Want:\t%s\n", want_json)
		t.Fail()
	}
}

// func TestAddCollection(t *testing.T) {
// 	var pwd = os.Getenv("PWD")
// 	var fp = strings.TrimSuffix(pwd, "/backend") + "/test/mock_collection.json"
// 	t.Log(fp)
// 	var mockCollection, err = os.Open(fp)
// 	if err != nil {
// 		t.Fatal(err)
// 	}

// 	DB = db.DBConnect()
// 	req, _ := http.NewRequest("POST", "http://localhost:5000/api/col/add", nil)
// 	req.Header.Add("Content-Type", "application/json")
// 	req.Body = mockCollection
// 	c := make([]byte, 1024)
// 	_, err = mockCollection.Read(c)
// 	if err != nil {
// 		t.Fail()
// 	}
// 	// t.Logf("%s\n", c)

// 	resp := httptest.NewRecorder()

// 	AddCollectionHandler(resp, req)

// 	if resp.Code != 302 {
// 		t.Errorf("AddCollection failed, got: %v", resp.Body.String())
// 	}
// }

// func TestAddMedia(t *testing.T) {
// 	var pwd = os.Getenv("PWD")
// 	var fp = strings.TrimSuffix(pwd, "/backend") + "/test/mock_media.json"
// 	t.Log(fp)
// 	var mockMedia, err = os.Open(fp)
// 	if err != nil {
// 		t.Fatal(err)
// 	}

// 	DB = db.DBConnect()
// 	req, _ := http.NewRequest("POST", "http://localhost:5000/api/col/1/media/add", nil)
// 	req.Header.Add("Content-Type", "application/json")
// 	req.Body = mockMedia
// 	c := make([]byte, 1024)
// 	_, err = mockMedia.Read(c)
// 	if err != nil {
// 		t.Fail()
// 	}
// 	// t.Logf("%s\n", c)

// 	resp := httptest.NewRecorder()

// 	AddMediaHandler(resp, req)

// 	if resp.Code != 302 {
// 		t.Errorf("AddMedia failed, got: %v", resp.Body.String())
// 	}
// }
