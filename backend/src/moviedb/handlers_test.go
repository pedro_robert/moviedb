package main

import (
	"reflect"
	"testing"
)

func AssertEqual(got interface{}, want interface{}, t *testing.T) {
	eq := reflect.DeepEqual(got, want)
	if !eq {
		t.Fatalf("fail:\nwant:\t%s\ngot:\t%s\n", want, got)
	}
}

func TestStringMapHelper(t *testing.T) {
	var m = []map[string]interface{}{
		{"_id": []byte("1"), "name": []byte("Test"), "description": []byte("Test test test")},
	}
	var want = []map[string]string{
		{"_id": "1", "name": "Test", "description": "Test test test"},
	}

	for i, v := range m {
		got := HelperParseToStringMap(v)
		AssertEqual(got, want[i], t)
	}
}
