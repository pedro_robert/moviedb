package main

import (
	"moviedb/db"
	"net/http"

	"github.com/go-zoo/bone"
)

/*
/api/*
	/collection/all							--> list all collections (GET)
	/collection/new							--> new collection (POST)
	/collection/"_id"/media/all				--> list all media in collection (GET)
	/collection/"_id"/media/add				--> add media to collection (POST)
	/collection/"_id"/media/"_id"			--> get media (GET)
	/collection/"_id"/media/"_id"/remove	--> remove media (GET)
	/collection/"_id"/media/"_id"/update	--> update media entry (POST)
*/
const (
	// DBcreds          = "dev:dev@tcp(127.0.0.1:3306)/moviedb"
	All             = "*"
	MediaTable      = "all_media"
	OwnedMediaTable = "owned_media"
	CollectionTable = "collections"

	MediaID      = "media_id"
	CollectionID = "collection_id"
)

var DB db.MovieDB

type Mux struct {
	*bone.Mux
}

func NewAPIRouter() *Mux {

	// test DB connection
	DB = db.DBConnect()

	sub := &Mux{
		bone.New(),
	}

	sub.GetFunc("", func(w http.ResponseWriter, req *http.Request) {
		w.Write([]byte("`Hello world` from movieDB API !"))
	})

	// Wrong Method Warnings
	sub.GetFunc("/col/new", NotPostWarningHandler)
	sub.GetFunc("/col/:cid/media/add", NotPostWarningHandler)
	sub.GetFunc("/col/:cid/media/:mid/update", NotPostWarningHandler)

	// Database API
	sub.GetFunc("/col/all", GetAllCollectionsHandler)
	sub.GetFunc("/col/:cid", GetCollectionHandler)
	sub.GetFunc("/col/:cid/media/all", GetAllMediaHandler)
	sub.GetFunc("/col/:cid/media/:mid", GetMediaHandler)
	sub.GetFunc("/col/:cid/media/:mid/remove", RemoveMediaHandler)

	sub.PostFunc("/col/add", AddCollectionHandler)
	sub.PostFunc("/col/:cid/media/add", AddMediaHandler)
	sub.PostFunc("/col/:cid/media/:mid/update", UpdateMediaHandler)

	// OMDB search shortcut
	sub.GetFunc("/omdb/:title", SearchOmdbHandler)

	// Raw Query endpoint
	sub.GetFunc("/raw", RawQueryHandler)

	return sub
}

func apiError(err error) *Response {
	resp := NewResponse(false)
	resp.SetData(err)
	return resp
}
