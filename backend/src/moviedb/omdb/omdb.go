package omdb

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"moviedb/db"
	"net/http"
	"strings"
)

type Response struct {
	ImdbID     string `json:"imdbID"`
	ImdbRating string `json:"imdbRating"`
	ImdbVotes  string `json:"imdbVotes"`
	Metascore  string `json:"Metascore"`
	Runtime    string `json:"Runtime"`
	Year       string `json:"Year"`
	Title      string `json:"Title"`
	Released   string `json:"Released"`
	Poster     string `json:"Poster"`
	Director   string `json:"Director"`
	Writer     string `json:"Writer"`
	Actors     string `json:"Actors"`
	Rated      string `json:"Rated"`
	Genre      string `json:"Genre"`
	Language   string `json:"Language"`
	Country    string `json:"Country"`
	Plot       string `json:"Plot"`
}

func Query(title string, year string) (*Response, error) {
	q := fmt.Sprintf("http://www.omdbapi.com/?t=%s&y=%s&plot=short&r=json", strings.Replace(title, " ", "+", -1), year)
	resp, err := http.Get(q)
	if err != nil {
		return nil, err
	}

	defer resp.Body.Close()
	b, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	m := &Response{}
	err = json.Unmarshal(b, m)
	if err != nil {
		return nil, err
	}

	return m, nil
}

// maybe do some parsing of the omdb.Response values to get typed fields
func (r *Response) toMovie() (*db.Media, error) {
	return nil, nil
}
