package main

import (
	"encoding/json"
	"fmt"
	"log"
)

// Response wraps the API response.
// It provides a status - if a request was handled successfully
type Response struct {
	Status bool        `json:"status"`
	Data   interface{} `json:"data"`
}

// RepsonseError wraps an error message
// We are able to return pretty error messages to the API user
type ResponseError struct {
	Error string `json:"error"`
}

// NewResponse is the type constructor with the success value.
// Returns a Repsonse object
func NewResponse(status bool) *Response {
	return &Response{Status: status}
}

// SetData converts whatever you feed it to a bytestream that acts as the Response data/payload
func (resp *Response) SetData(data interface{}) {
	switch data.(type) {
	case error:
		err := data.(error)
		resp.Data = ResponseError{Error: err.Error()}
	default:
		resp.Data = data
	}
}

// IsSuccess returns the status of the Response as a readable string
func (resp *Response) IsSuccess() string {
	if !resp.Status {
		return "failure"
	}
	return "success"
}

// MarshalJSON implements the Marshaler interface for JSON format
func (resp *Response) MarshalJSON() ([]byte, error) {
	data_out, err := json.Marshal(resp.Data)
	out := fmt.Sprintf("{\"status\":\"%s\",\"data\":%s}", resp.IsSuccess(), data_out)
	return []byte(out), err
}

// ToJSON converts the Repsonse type to JSON format
func (resp *Response) ToJSON() []byte {
	json, err := json.Marshal(resp)
	if err != nil {
		log.Println("marshaling Response failed")
	}
	return json
}
